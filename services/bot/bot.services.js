class Bot {

    startBot = () => {
        return 'Начинаем чемпионат мира по клавогонкам. Ожидаем учасников'
    };

    startGameBot = () => {
        return 'Начинаем гонку'
    };

    joinPlayer = (username) => {
        return `${username} присоеденился к чемпионату`
    };

    leavePlayer = (username) => {
        return `${username} покинул чемпионат`
    };

    startLoadingGame = () => {
        return `Все учасники готовы, через 10 секунд стартуем`
    };

    currentResult = (listRate, secondsToFinish)=> {
        const getCurrentRate = this.getCurrentRate(listRate);
        const text = `До конца гонки осталось ${secondsToFinish}сек.<br>`;
        return listRate.reduce((prev, user, index) => prev + getCurrentRate(user.name, index), text)
    };

    finishResult = listRate => {
        const getCurrentRate = this.getFinishRate(listRate);

        return listRate.reduce((prev, user, index) => prev + getCurrentRate(user.name, index), '')
    };

    getFinishRate = (listRate) => {
        const lastPlace = listRate.length - 1;
        return (username, place) => {
            switch (place) {
                case 0:
                    return `Первым пересек финишную прямую ${username}<br>`;
                case 1:
                    return `${username} второй<br>`;
                case 2:
                    return (2 === lastPlace) ? `Последний ${username}<br>` : `${username} третий<br>`;
                case 3:
                    return (3 === lastPlace) ? `Последний ${username}<br>` : `${username} четвертый<br>`;
                case 4:
                    return (4 === lastPlace) ? `Последний ${username}<br>` : `${username} пятый, далее по списку<br>`;
                default :
                    return `${place} - ${username}`
            }
        }
    };

    getCurrentRate = (listRate) => {
        const lastPlace = listRate.length - 1;
        return (username, place) => {
            switch (place) {
                case 0:
                    return `Первым едет ${(0 === lastPlace) ? username + ', интересно с кем он соревнуется' : username}<br>`;
                case 1:
                    return `${username} пока второй<br>`;
                case 2:
                    return (2 === lastPlace) ? `Последним едет ${username}<br>` : `${username} третий<br>`;
                case 3:
                    return (3 === lastPlace) ? `Последним едет ${username}<br>` : `${username} четвертый<br>`;
                case 4:
                    return (3 === lastPlace) ? `Последним едет ${username}<br>` : `${username} пятый, далее по списку<br>`;
                default :
                    return `${place} - ${username}`
            }
        };
    };
}


export default Bot