class Players {
    constructor(){
        this.players = [];
    }

    createPlayer = (username) => ({
        name: username,
        status: false,
        progress: 0,
        percentageProgress: 0,
        finished: 0
    });

    addPlayer = (username) => {
        this.players = [...this.players, this.createPlayer(username)]
    };

    deletePlayer = (username) => {
        this.players =  this.players.filter(player => player.name !== username)
    };

    getPlayers = () => this.players;

    findPlayer = (username) => {
        return this.players.find(player => player.name === username)
    };

    getStatusPlayers = (username) => {
        return this.findPlayer(username).status
    };

    getPercentageProgress = (username) => {
        return this.findPlayer(username).percentageProgress
    };

    changePlayerStatus = (username) => {
        this.players = this.players.map(player => {
            if (player.name === username) {
                player.status = !player.status
            }
            return player
        });
    };

    changePlayerProgress = (username, target) => {
        this.players = this.players.map(player => {
            if (player.name === username) {
                player.progress += 1;
                player.percentageProgress = Math.floor((player.progress / target) * 100)
            }
            return player
        })
    };

    checkStatusPlayers = () => {
        if(this.players.length === 1){
            return false
        }
        return !this.players.find(player => player.status === false)
    };

    checkFinishedPlayer = (username, target) => {
        this.players = this.players.map(player => {
            if (player.name === username && player.progress === target) {
                player.finished = new Date();
            }
            return player
        })
    };

    checkFinishedAllPlayer = () => {
        return !this.players.find(player => player.finished === 0)
    };

    setFinishTime = (timeFinished) => {
        this.players = this.players.map(player => {
            if (!player.finished) {
                player.finished = timeFinished;
            }
            return player
        })
    };

    getListRating = () => {
        const currentTime = new Date();
        return this.players.sort((a, b) => {
            const finishedA = a.finished ? a.finished : currentTime;
            const finishedB = b.finished ? b.finished : currentTime;
            if ((finishedA - a.progress) > (finishedB - b.progress)) {
                return 1
            }
            if ((finishedA - a.progress) < (finishedB - b.progress)) {
                return -1
            }
            if ((finishedA - a.progress) === (finishedB - b.progress)) {
                return 0
            }
        })
    };
}

export default Players;
