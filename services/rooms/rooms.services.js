import {texts} from "../../data";
import Players from "../players/players.services";

class Rooms {
    constructor(maximumUserInRoom){
        this.rooms = []
        this.maximumUserInRoom = maximumUserInRoom
    }
    
    createRoom = (roomId) => ({
        roomId,
        status: false,
        players: new Players(),
        target: 0
    });

    addRoom = (roomId) => {
        this.rooms = [...this.rooms, this.createRoom(roomId)]
    };

    deleteRoom = (roomId) => {
        this.rooms = this.rooms.filter(room => roomId !== room.roomId)
    };

    getRooms = () => {
        return this.rooms.map(room => {
            return {
                ...room,
                players: room?.players.getPlayers()
            }
        })
    };

    getRoom = (roomId) => {
        const room = this.rooms.find(room => room.roomId === roomId);
        const room2 = {
            ...room,
            players: room?.players.getPlayers() || []
        }
        return room2
    };

    findRoom = (roomId) => {
        return this.rooms.find(room => roomId === room.roomId)
    };

    joinPlayerInRoom = (roomId, username) => {
        this.rooms = this.rooms.map(room => {
            if (room.roomId === roomId) {
                room.players.addPlayer(username);
            }
            return room;
        });
    };

    findStatusPlayer = (roomId, username) => {
        return this.findRoom(roomId).players.getStatusPlayers(username)
    };

    deletePlayerInRoom = (roomId, username) => {
        this.rooms = this.rooms.map(room => {
            if (room.roomId === roomId) {
                room.players.deletePlayer(username)
            }
            return room
        })
    };

    changeUserStatus = (roomId, username) => {
        this.rooms = this.rooms.map(room => {
            if (room.roomId === roomId) {
                room.players.changePlayerStatus(username)
            }
            return room
        })
    };

    changeUserProgress = (roomId, username) => {
        this.rooms = this.rooms.map(room => {
            if (room.roomId === roomId) {
                room.players.changePlayerProgress(username, room.target)
            }
            return room
        })
    };

    getPercentageProgress = (roomId, username) => {
        return this.findRoom(roomId).players.getPercentageProgress(username);
    };

    checkReadyAllPlayers = (roomId) => {
        const currentRoom = this.findRoom(roomId);
        return currentRoom.players.checkStatusPlayers();
    };

    changeStatusGame = (roomId, status, idText) => {
        this.rooms = this.rooms.map(room => {
            if (room.roomId === roomId) {
                room.status = status;
                room.target = texts[idText].length
            }
            return room;
        })
    };

    availableRoom = () => this.getRooms().filter(room => (room.players.length < this.maximumUserInRoom && !room.status));

    checkFinishedPlayer = (roomId, username) => {

        this.rooms = this.rooms.map(room => {
            if (room.roomId === roomId) {
                room.players.checkFinishedPlayer(username, room.target)
            }
            return room
        })
    };

    checkFinishGame = (roomId) => {
        return this.findRoom(roomId)?.players.checkFinishedAllPlayer();
    };

    finishGame = (roomId) => {
        const timeFinished = new Date();
        this.rooms = this.rooms.map(room => {
            if (room.roomId === roomId) {
                room.players.setFinishTime(timeFinished)
            }
            return room
        })
    };

    getListRating = (roomId) => {
        const room = this.findRoom(roomId);
        return room?.players.getListRating()
    };
}

export default Rooms