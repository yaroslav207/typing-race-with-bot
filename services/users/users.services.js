class Users {
    constructor() {
        this.users = []
    }

    checkUser = (username) => {
        return this.users.includes(username)
    };

    addUserList = (username) => {
        this.users = [...this.users, username];
    };

    removeUserList = (username) => {
        this.users = this.users.filter(user => user !== username)
    };
}

export default Users;