import {createElement} from "../../helper.mjs";

export const createBotBlock = () => {
    const botBlock = createElement({
        tagName: 'div',
        className: 'bot-block',
    });

    botBlock.innerHTML = `
        <img src="../../../images/commentator.png" class="commentator-img">
        <div class="comment-text"></div> 
    `;

    return botBlock
};

export const addComment = (text) => {
    const commentBlock = document.querySelector('.comment-text');
    if(!commentBlock){
        return
    }

    commentBlock.innerHTML = `<p>${text}</p>`
};

