import {createElement} from "../../helper.mjs";

// Factory
export class RoomBlockFactory {

    createJoinButton = (onClick) => {
        const joinButton = createElement({
            tagName: 'button',
            className: 'join-btn',
        });

        joinButton.addEventListener('click', onClick);
        joinButton.innerText = 'JOIN';

        return joinButton
    };

    create(roomInfo, joinRoom){
        const roomBlock = createElement({
            tagName: 'div',
            className: 'room',
            attributes: {
                id: roomInfo.roomId
            }
        });

        const joinButton = this.createJoinButton(joinRoom);

        roomBlock.innerHTML = (`
    <span class="count-user">${roomInfo.players.length} users connected</span>
    <span class="title-room">${roomInfo.roomId}</span>
  `);
        roomBlock.appendChild(joinButton);

        return roomBlock;
    };
}

