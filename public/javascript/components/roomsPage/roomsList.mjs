import {createElement} from "../../helper.mjs";
import { RoomBlockFactory } from "./roomBlock.mjs"

const roomBlockFactory = new RoomBlockFactory();

export const createRoomList = () => {
    const roomsList = createElement({
        tagName: 'div',
        className: 'list-rooms',
    });

    return roomsList;
};

export const fillRoomList = (rooms, joinRoom) => {
    const roomsListBlock = document.querySelector('.list-rooms');
    if (!roomsListBlock){
        return
    }
    roomsListBlock.innerHTML = '';

    rooms.forEach( roomInfo => {
        roomsListBlock.appendChild(roomBlockFactory.create(roomInfo, joinRoom(roomInfo.roomId)))
    });
};