
export const getCurrentRoomId = (socket, rooms) => (
    Object.keys(socket.rooms)
        .find(roomId => rooms
            .find(room => room.roomId === roomId))
);

export const randomValue = () => {
    return Math.floor(Math.random() * 6)
};

