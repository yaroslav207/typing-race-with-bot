FROM node:14.15.1
RUN mkdir -p /app/
WORKDIR /app/

COPY package.json package.json
COPY package-lock.json package-lock.json

RUN npm install

COPY . .

EXPOSE 3002

CMD [ "node", "server.js" ]