import * as config from "./config";
import {getCurrentRoomId, randomValue} from "../helpers";


import {Users, Rooms, Bot} from "../services/"

const users = new Users();
const rooms = new Rooms(config.MAXIMUM_USERS_FOR_ONE_ROOM);
const bot = new Bot();

const leaveRoom = (io, socket, username) => {
    const currentId = getCurrentRoomId(socket, rooms.getRooms());
    if (!currentId) {
        return
    }

    socket.leave(currentId, () => {
        const currentRoom = rooms.getRoom(currentId);
        if (currentRoom.players.length === 1) {
            rooms.deleteRoom(currentId);
        } else {
            rooms.deletePlayerInRoom(currentId, username);
            io.to(currentId).emit("UPDATE_USERS_IN_ROOM", rooms.getRoom(currentId));
            io.to(currentId).emit("COMMENT_BOT", bot.leavePlayer(username));

            if (rooms.checkReadyAllPlayers(currentId) && !currentRoom.status) {
                startLoadingGame(io, currentId)
            }

          if(currentRoom.status && rooms.checkFinishGame(currentId)){
            rooms.finishGame(currentId)
          }
        }

        io.emit("UPDATE_ROOMS", rooms.availableRoom());
    });
};
const startGame = (io, roomId, idText) => {
    let secondsForGame = config.SECONDS_FOR_GAME;

    io.to(roomId).emit("START_GAME", secondsForGame);
    io.to(roomId).emit("COMMENT_BOT", bot.startGameBot());
    rooms.changeStatusGame(roomId, true, idText);

    let timerGameId = setInterval(() => {
      if(rooms.checkFinishGame(roomId)){
        clearInterval(timerGameId);
        return
      }
        io.to(roomId).emit("CHANGE_TIMER_FOR_GAME", --secondsForGame);
        if (secondsForGame === 30){
            io.to(roomId).emit("COMMENT_BOT", bot.currentResult(rooms.getListRating(roomId), secondsForGame));
        }
    }, 1000);

    setTimeout(() => {
        clearInterval(timerGameId);
        rooms.finishGame(roomId);
        io.to(roomId).emit("FINISH_GAME", rooms.getListRating(roomId));
        io.to(roomId).emit("COMMENT_BOT", bot.finishResult(rooms.getListRating(roomId), secondsForGame));
    }, secondsForGame * 1000);
};
const startLoadingGame = (io, roomId) => {
    let secondBeforeStartGame = config.SECONDS_TIMER_BEFORE_START_GAME;

    const idText = randomValue();
    io.to(roomId).emit("START_TIMER", {idText, secondBeforeStartGame});
    io.to(roomId).emit("COMMENT_BOT", bot.startLoadingGame());

    let timerId = setInterval(() => {
        io.to(roomId).emit("CHANGE_TIMER", --secondBeforeStartGame);
    }, 1000);

    setTimeout(() => {
        clearInterval(timerId);
        startGame(io, roomId, idText);
    }, config.SECONDS_TIMER_BEFORE_START_GAME * 1000);
};

export default io => {
    io.on("connection", socket => {
        const username = socket.handshake.query.username;

        if (users.checkUser(username)) {
            socket.emit("ERROR_LOGIN", 'this login already exists');
            return
        }
        users.addUserList(username);

        socket.emit("UPDATE_ROOMS", rooms.availableRoom());

        socket.on("CREATE_ROOM", roomId => {
            if (!roomId) {
                return;
            }
            const checkRoom = rooms.hasOwnProperty(roomId);
            if (checkRoom) {
                socket.emit("ERROR", "Комната с таким названием уже существует");
                return;
            }

            rooms.addRoom(roomId);
            rooms.joinPlayerInRoom(roomId, username);

            socket.join(roomId, () => {
                const roomInfo = rooms.getRoom(roomId);
                socket.emit("JOIN_ROOM_DONE", roomInfo);
                io.to(roomId).emit("COMMENT_BOT", bot.joinPlayer(username));
                socket.emit("COMMENT_BOT", bot.startBot());
            });

            io.emit("UPDATE_ROOMS", rooms.availableRoom());
        });

        socket.on("JOIN_ROOM", roomId => {
            const prevRoomId = getCurrentRoomId(socket, rooms.getRooms());
            if (roomId === prevRoomId) {
                return;
            }
            if (prevRoomId) {
                socket.leave(prevRoomId);
            }
            const room = rooms.getRoom(roomId);
            if(room.status){
              return;
            }
            rooms.joinPlayerInRoom(roomId, username);

            socket.join(roomId, () => {
                io.to(socket.id).emit("JOIN_ROOM_DONE", rooms.getRoom(roomId));
                io.emit("UPDATE_ROOMS", rooms.availableRoom());
                io.to(roomId).emit("UPDATE_USERS_IN_ROOM", rooms.getRoom(roomId));
                io.to(roomId).emit("COMMENT_BOT", bot.joinPlayer(username));
                socket.emit("COMMENT_BOT", bot.startBot());
            });

        });

        socket.on("LEAVE_ROOM", () => {
            leaveRoom(io, socket, username);
            socket.emit("LEAVE_ROOM_DONE");
            socket.emit("UPDATE_ROOMS", rooms.availableRoom());
        });

        socket.on("CHANGE_USER_STATUS", () => {
            const roomId = getCurrentRoomId(socket, rooms.getRooms());
            if (!roomId) {
                return
            }

            rooms.changeUserStatus(roomId, username);
            io.to(roomId).emit("UPDATE_USERS_IN_ROOM", rooms.getRoom(roomId));
            socket.emit("CHANGE_USER_STATUS", rooms.findStatusPlayer(roomId, username));

            if (rooms.checkReadyAllPlayers(roomId)) {
                startLoadingGame(io, roomId)
            }
        });

        socket.on("CHANGE_PROGRESS", () => {
            const roomId = getCurrentRoomId(socket, rooms.getRooms());
            rooms.changeUserProgress(roomId, username);
            io.to(roomId).emit("CHANGE_PROGRESS_USER", {
                userId: username,
                progress: rooms.getPercentageProgress(roomId, username)
            });
            rooms.checkFinishedPlayer(roomId, username);
            if(rooms.checkFinishGame(roomId)){
              rooms.finishGame(roomId);
              io.to(roomId).emit("COMMENT_BOT", bot.finishResult(rooms.getListRating(roomId)));
            }
        });

        socket.on("disconnecting", () => {
            leaveRoom(io, socket, username);
            users.removeUserList(username);
            socket.emit("UPDATE_ROOMS", rooms.availableRoom());
        });
    });
};
